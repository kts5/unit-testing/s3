function factorial(n){
	if(typeof n != 'number') return undefined;
	if(n < 0) return undefined;
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
};

// console.log(factorial(5));


function addition(n1, n2) {

	// must always use return to be read by the tester
	return n1 + n2;
};


function result() {
	return true;
}

function div_check(x) {
	if (x % 5 == 0) {
		return true;
	} else if (x % 7 == 0){
		return true;
	} else {
		return false;
	}
};


const names = {
	"Brandon": {
		"name": "Brandon Boyd",
		"age": 35
	},
	"Steve": {
		"name": "Steve Tyler",
		"age": 56
	}
};


module.exports = {
	factorial,
	addition,
	result,
	div_check,
	names
};